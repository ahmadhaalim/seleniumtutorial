package testpackage;

import home.driver.WebDriverInstance;
import home.pages.JoinPage;
import home.pages.MainPage;
import org.junit.Assert;
import org.junit.Test;


public class TestRunner {
    public void start(){
        WebDriverInstance.initialize();
    }

    public void quit(){
        WebDriverInstance.quit();
    }

    public void testCase1(){
        WebDriverInstance.driver.get("https://agate.id");
        MainPage page = new MainPage();
        JoinPage pageJoin = new JoinPage();
        page.clickJoinAgate();
        String actual = pageJoin.isOnPage();
        Assert.assertEquals("EXPLORE OPPORTUNITIES", actual);
        System.out.println("True");
    }

    public static void main(String[] args) {
        TestRunner run = new TestRunner();
        run.start();
        run.testCase1();
        run.quit();
    }
}
