package home.pages;

import home.driver.WebDriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class JoinPage {
    public String isOnPage(){
        WebElement element = WebDriverInstance.driver.findElement(By.className("btn-white"));
        return element.getText();
    }
}
