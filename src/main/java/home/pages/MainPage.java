package home.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static home.driver.WebDriverInstance.driver;

public class MainPage {
    public void clickJoinAgate(){
        WebElement element = driver.findElement(By.xpath("//a[contains(text(),'JOIN AGATE')]"));
        element.click();
    }

    public boolean isOnPage(){
        WebElement element = driver.findElement(By.className("btn btn-agate"));
        return element.isDisplayed();
    }
}
